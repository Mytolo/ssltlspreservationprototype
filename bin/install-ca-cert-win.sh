#!/usr/bin/env bash
# This script injects a ca-cert into a non-running windows disk image in 
# qcow2 or raw format
# needed software lib32-nss, qemu, vde2, ...


echoerr() { echo -e "$@" 1>&2; }

printUsage() {
      cat << EOF
      USAGE: install-ca-cert.sh <flags>
      Flags:
               -i|--image:        Path to image, currently the formats qcow2 and raw are supported
               -c|--cert:         Path to CA Certificate
               -n|--cert-name:    Name of CA Certificate showed in Distribution
               -r|--reg-file:     Exported registry file needed for inserting into HKEY_LOCAL_MACHINE/SOFTWARE hive

      Optional Flags:
               -p|--mount-path:   Path the image should be mounted to. Default is /mnt

EOF
}

while (( "$#" ))
do
  case "$1" in
    -i|--image)
      IMGPATH="$2"
      shift 2
      ;;
    -c|--cert)
      certificate="$2"
      shift 2
      ;;
    -n|--cert-name)
      certName="$2"
      shift 2
      ;;
    -p|--mount-path)
      mntDir=$2
      shift 2
      ;;
    -r|--reg-file)
      regFile=$2
      shift 2
      ;;
    --) #end of argument parsing
      shift
      break
      ;;
    -*|--*-) # unsupported flags
      printUsage
      exit 1
      ;;
    *) # preserver positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

if [[ -z $mntDir ]]
then
  mntDir=/mnt
fi

eval set -- "$PARAMS"

# check for required parameters
if [ "x$IMGPATH" == "x" ] || \
   [ "x$certificate" == "x" ] || \
   [ "x$certName" == "x" ] || \
   [ "x$regFile" == "x" ]
then
  printUsage
  exit 1
fi

# =============================================================================
# Default root / argument check 
# ==============================================================================

# get root privileges
[ "$UID" -eq 0 ] || exec sudo "$0" -r "$regFile" -i "$IMGPATH" -c "$certificate" -n "$certName" -p "$mntDir" "$@"
echo "Registry File: $regFile"


# ==============================================================================
# mount the image
./bin/mount-disk-image.sh -i "$IMGPATH" -p "$mntDir"


# =============================================================================
# inject the cert to the registry to the windows hive:
# HKEY_LOCAL_MACHINE/software
# edit registry with hiveexregedit from hivex 
# the reg file is extracted from a system where the cert is installed succesfully
# and converted in linux style with  iconv -f utf-16le -t utf-8 < <win.reg> | dos2unix > <linux.reg>
testPaths=(Windows/System32/config/SOFTWARE Windows/System32/config/software Windows/system32/config/software WINDOWS/system32/config/software WINNT/system32/config/software)
for path in "${testPaths[@]}"
do
  if [ -f "$mntDir/$path" ]
  then
    echo "found $path"
    hivexregedit --merge "$mntDir/$path" "$regFile"
  fi
done

CERT=$(awk -v RS='-----BEGIN CERTIFICATE-----.*-----END CERTIFICATE-----' 'RT{print RT}' "$certificate")
base=$(basename "$certificate")
echo "$CERT" > "$mntDir/${base%.*}.crt"


# =============================================================================
# add certificate to the cert database of firefox, chrome, etc ...
# cert8 (legacy - DBM)
# For windows 2000 and 98 this is necessary because windows won't accept ca certificates
function nssCert {
  subpath=$1
  if [ -d "$mntDir/$subpath" ]
  then
    OI=$IFS
    IFS=""
    echo $(find "$mntDir/$subpath" -name "cert8.db")
    for cDB in $(find "$mntDir/$subpath" -name "cert8.db")
    do
      echo $cDB
      certdir=$(dirname $cDB)
      if [ -d "$certdir" ]
      then
        echo "Found certdatabase $cDB"
        docker run \
          -v $mntDir:$mntDir \
          -v $(dirname $certificate):/certs\
          libnss\
          certutil -A -n "$certName" -t "TCu,Cu,Tu" -i "/certs/$(basename $certificate)"  -d "dbm:$certdir"
      fi
    done
    for cDB in $(find "$mntDir/$subpath" -name "cert9.db")
    do
      echo "Found certdatabase cert9: $cDB"
      cp $cDB $cDB.orig
      certdir=$(dirname $cDB)
      certutil -A -n "$certName" -t "TCu,Cu,Tu" -i $certificate -d sql:$certdir
    done
  fi
  IFS=$OI
}

nssCert "Users"
nssCert "Dokumente und Einstellungen"
nssCert "WINDOWS/Application Data"

# enable windows root trust to firefox
FFPaths=("Program Files (x86)/Mozilla Firefox" "Programme/Mozilla Firefox")
for ffpath in "${FFPaths[@]}"
do
  if [ -d "$mntDir/$ffpath" ]
  then 
    echo "Fonnd Mozilla Firefox Path: $mntDir/$ffpath"
    echo "copying firefox config files enabling root ca store on firefox"
    cp defaults_pref_local-settings.js "$mntDir/$ffpath/defaults/pref/local-settings.js" -v
    cp  enable_root_certs.cfg "$mntDir/$ffpath" -v
  fi
done

./bin/umount-image.sh -p "$mntDir"



echo -e """
\x1b[32m================================================================================
SUCCESSFULLY INSTALLED THE CA CERTIFICAT TO THE IMAGE

        Imagepath:      $IMGPATH
        Certificate:    $certificate
        Name of Cert:   $certName
        Registry File:  $regFile

================================================================================
\x1b[0m
"""
