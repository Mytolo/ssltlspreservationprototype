#! /usr/bin/env bash
# bash parsing: https://medium.com/@Drew_Stokes/bash-argument-parsing-54f3b81a6a8f
PARAMS=""

printUsage() {
      cat << EOF
      USAGE: start-disk-image.sh <flags>
      Flags:
               -i|--image:        Path to image
               -f|--format:       Format of Image
               -c|--num-cpu:      Amount of cpu cores
               -m|--memory:       Amount of RAM in MB

      Optional Flags:
               -o||--other-args:  string of qemu args e.g. for networking
                                  example: -netdev user,id=n0 -device rtl8139,netdev=n0
EOF
}

while (( "$#" ))
do
  case "$1" in
    -i|--image)
      IMAGE="$2"
      shift 2
      ;;
    -f|--format)
      FORMAT="$2"
      shift 2
      ;;
    -c|--num-cpu)
      NUMCPU="$2"
      shift 2
      ;;
    -m|--memory)
      M="$2"
      shift 2
      ;;
    -o|--other-args)
      OTHER="$2"
      shift 2
      ;;
    --) #end of argument parsing
      shift
      break
      ;;
    -*|--*-) # unsupported flags
      printUsage
      exit 1
      ;;
    *) # preserver positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

eval set -- "$PARAMS"

if [ "x$IMAGE" == "x" ]
then
  IMGPATH=/run/media/christop/SSD/Bachelorarbeit/QEMU-img/
  IMAGE="$(du -h $IMGPATH/* | awk '{print $2}' | fzf)"
fi

if  [ "x$NUMCPU" == "x" ] || \
    [ "x$FORMAT" == "x" ] || \
    [ "x$IMAGE" == "x" ] || \
    [ "x$M" == "x" ]
then
  printUsage
  exit 1
fi


# qemu-system-x86_64 -enable-kvm -drive file=$1,format=qcow2 -m $2 -smp 3
echo qemu-system-x86_64 -enable-kvm -drive file=$IMAGE,format=$FORMAT -m $M -smp $NUMCPU $OTHER
qemu-system-x86_64 -enable-kvm -drive file=$IMAGE,format=$FORMAT -m $M -smp $NUMCPU $OTHER
