#!/usr/bin/env bash

PARAMS=""

printUsage() {
      cat << EOF
      USAGE: reset-images-to-default.sh <flags> <source> <destination>
      Flags:
               -y|-silent:  Do not ask for confirmation.
      Positional Arguments:
               source:      Backup source directory
               destination: Destination directory
EOF
}

while (( "$#" ))
do
  case "$1" in
    -y|--silent)
      ASK=1
      shift 1
      ;;
    -t|--dest)
      IMAGEDIR="$2"
      shift 2
      ;;
    --) #end of argument parsing
      shift
      break
      ;;
    -*|--*-) # unsupported flags
      printUsage
      exit 1
      ;;
    *) # preserver positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

eval set -- "$PARAMS"
BACKUPDIR="$1"
IMAGEDIR="$2"

if  [ "x$IMAGEDIR" == "x" ] || \
    [ "x$BACKUPDIR" == "x" ]
then
  printUsage
  exit 1
fi

if [ "$ASK" != "1" ]
then
  echo "Do you want to sync $BACKUPDIR to $IMAGEDIR? (y/N)"
  read a
  if [ "$a" != "y" ] && [ "$a" != "Y" ]
  then
    exit 0
  fi
fi

echo rsync -v -a --progress $BACKUPDIR $IMAGEDIR
rsync -v -a --progress $BACKUPDIR $IMAGEDIR
