#!/usr/bin/env bash

PARAMS=""

printUsage() {
      cat << EOF
      USAGE: install-ca-cert-to-images.sh <flags>
      Flags:
               -d|--image-dir:     path to qemu image root dir with a Linux and a
                                   Windows Folder containing the systems
EOF
}

SUFFIX=".results"
PORT="1"
while (( "$#" ))
do
  case "$1" in
    -d|--image-dir)
      IMAGEPATH="$2"
      shift 2
      ;;
    --) #end of argument parsing
      shift
      break
      ;;
    -*|--*-) # unsupported flags
      printUsage
      exit 1
      ;;
    *) # preserver positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

eval set -- "$PARAMS"

if  [ "x$IMAGEPATH" == "x" ]
then
  printUsage
  exit 1
fi
IMAGEPATH=$(echo $IMAGEPATH | sed 's/\/$//g')

echo "THIS SCRIPT IS GOING TO INSTALL THE PROTOTYPE CA CERTIFICATE IN THE FOLDERS $IMAGEPATH/Windows, $IMAGEPATH/Linux"
echo "THE IMAGE SUFFIXES MAY .img and .raw"
echo "DO YOU WANT TO CONTINUE? (PRESS q,x for exit other for execution)"

read a
if [ "$a" == "x" ] || [ "$a" == "q" ]
then
  exit 0
fi

for path in $(ls $IMAGEPATH/Windows/*.img)
do
  echo -e "\x1b[34mInstalling certificate to $(basename $path)\x1b[0m"
  ./bin/install-ca-cert-win.sh -i $path -r $(pwd)/PyWbHTTPSProxyCACert-lin.reg -c $(pwd)/pywb/proxy-certs/pywb-ca.pem -n "PyWb"
done

exit 0

for path in $(ls $IMAGEPATH/Linux/*.img)
do
  echo "installing certificate to $(basename $path)"
  ./bin/install-ca-cert.sh -c $(pwd)/pywb/proxy-certs/pywb-ca.pem -n PyWb -i $path
done
# one minute has to go after the certificate had been installed
sleep 60
